module go-spacexbot

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/jmoiron/sqlx v1.3.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nicksnyder/go-i18n/v2 v2.1.2
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.0
	golang.org/x/text v0.3.3
	gopkg.in/tucnak/telebot.v2 v2.3.5
)
