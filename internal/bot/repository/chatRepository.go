package repository

import (
	"go-spacexbot/internal/bot/dao"
	"go-spacexbot/internal/bot/models"
)

type chatRepository struct {
	d dao.ChatDao
}

func (r chatRepository) ById(id int64) (*models.Chat, error) {
	return r.d.ById(id)
}
