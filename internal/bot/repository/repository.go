package repository

import (
	"errors"
	"go-spacexbot/internal/bot/dao"
)

type Repository struct {
	d            dao.Dao
	chat         *chatRepository
	subscription *subscriptionRepository
}

func New(datasource, databaseUrl string) (*Repository, error) {
	var d dao.Dao
	switch datasource {
	case "mysql":
		mysql, err := dao.NewMysql(databaseUrl)
		if err != nil {
			return nil, err
		}
		d = mysql
	default:
		return nil, errors.New("unknown datasource " + datasource)
	}
	return &Repository{
		d:            d,
		chat:         &chatRepository{d: d.Chat()},
		subscription: &subscriptionRepository{d: d.Subscriptions()},
	}, nil
}

func (r Repository) Chat() *chatRepository {
	return r.chat
}

func (r Repository) Subscription() *subscriptionRepository {
	return r.subscription
}
