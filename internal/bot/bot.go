package bot

import (
	"github.com/sirupsen/logrus"
	"go-spacexbot/assets"
	"go-spacexbot/internal/bot/models"
	repository2 "go-spacexbot/internal/bot/repository"
	tb "gopkg.in/tucnak/telebot.v2"
	"time"
)

type TelegramBot struct {
	tg   *tb.Bot
	repo *repository2.Repository

	bundles map[string]assets.Localize
	logger  *logrus.Logger
	api     *SpaceXDataApi
}

type Session struct {
	models.Chat
	b        *TelegramBot
	Localize assets.Localize
}

func (s *Session) processError(err error) {
	s.b.logger.Error(err)
	_, err = s.b.tg.Send(&tb.User{ID: int(s.ID)}, ":(")
	if err != nil {
		s.b.logger.Errorf("Cannot send message: %s", err)
	}

}

func NewTelegramBot(repo *repository2.Repository, logger *logrus.Logger, bundles map[string]assets.Localize, token string) (*TelegramBot, error) {
	bot, err := tb.NewBot(tb.Settings{
		Token:     token,
		Poller:    &tb.LongPoller{Timeout: 10 * time.Second},
		ParseMode: tb.ModeHTML,
	})
	if err != nil {
		return nil, err
	}
	return &TelegramBot{
		tg:      bot,
		repo:    repo,
		bundles: bundles,
		logger:  logger,
		api:     NewSpaceXDataApi(),
	}, nil
}

func (b *TelegramBot) Start() {
	b.tg.Handle(tb.OnText, func(m *tb.Message) {
		s, err := newSession(m, b)
		if err != nil {
			b.logger.Warn(err)
			return
		}

		switch m.Text {
		case "/start":
			b.handlerStart(s, m)
		case "/help":
			b.handlerHelp(s, m)
		case "/keyboard":
			b.handlerKeyboard(s, m)
		case "/get":
			b.handlerGet(s, m)
		case "/subscribe":
			b.handlerSubscribe(s, m)
		case "/extend":
			b.handlerExtend(s, m)
		case "/next", s.Localize("KeyboardNext"):
			b.handlerNext(s, m)
		case "/upcoming", s.Localize("KeyboardUpcoming"):
			b.handlerUpcoming(s, m)
		case "/pic", "/picture", s.Localize("KeyboardPicture"):
			b.handlerPicture(s, m)
		case "/settings", s.Localize("KeyboardSettings"):
			b.handlerSettings(s, m)
		case "/last", s.Localize("KeyboardLastTweet"):
			b.handlerLast(s, m)
		case "/stream", s.Localize("KeyboardStreamLink"):
			b.handlerStream(s, m)
		}
	})
	b.tg.Start()
}

func newSession(m *tb.Message, b *TelegramBot) (*Session, error) {
	chat, err := b.repo.Chat().ById(m.Chat.ID)
	if err != nil {
		return nil, err
	}
	var localize assets.Localize
	if l, ok := b.bundles[chat.Language]; ok {
		localize = l
	} else {
		localize = b.bundles["en"]
	}
	s := &Session{
		b:        b,
		Chat:     *chat,
		Localize: localize,
	}
	return s, nil
}
