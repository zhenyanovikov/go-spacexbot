package mysql

import (
	"github.com/jmoiron/sqlx"
	"go-spacexbot/internal/bot/models"
)

type SubscriptionDao struct {
	db *sqlx.DB
}

func NewSubscriptionDao(db *sqlx.DB) *SubscriptionDao {
	return &SubscriptionDao{db: db}
}

func (d SubscriptionDao) ById(chatId int64) ([]models.Subscription, error) {
	panic("implement me")
}

func (d SubscriptionDao) Update(subscription *models.Subscription) error {
	panic("implement me")
}

func (d SubscriptionDao) Delete(subscription *models.Subscription) error {
	panic("implement me")
}

func (d SubscriptionDao) AmountById(chatId int64) (int, error) {
	panic("implement me")
}
