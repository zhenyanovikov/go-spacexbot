package dao

import (
	"go-spacexbot/internal/bot/models"
)

type Dao interface {
	Chat() ChatDao
	Subscriptions() SubscriptionDao
}

type ChatDao interface {
	ById(id int64) (*models.Chat, error)
	AllWithNotify(notify bool) ([]models.Chat, error)

	Update(c *models.Chat) error

	Delete(id int) error
}

type SubscriptionDao interface {
	ById(chatId int64) ([]models.Subscription, error)
	Update(*models.Subscription) error
	Delete(*models.Subscription) error
	AmountById(chatId int64) (int, error)
}
