package models

import "time"

type Chat struct {
	ID               int64
	ToNotify         bool   `db:"to_notify"`
	Language         string `db:"language"`
	LastRequestTime  time.Time
	RegisterTime     time.Time
	MessagesSent     int
	IsAdmin          bool
	Extended         bool
	TweetReplaceMode bool
	Subscribes       *[]Subscription
}

type Subscription struct {
	ChatID         int64  `gorm:"primary_key;AUTO_INCREMENT:false"`
	SubscriptionID int64  `gorm:"primary_key;AUTO_INCREMENT:false"`
	Name           string `gorm:"not null"`
	ScreenName     string `gorm:"not null"`
	Enabled        bool   `gorm:"not null;default:true"`
}

type Mission struct {
	Title      string
	Customer   string
	NetStatus  string
	LaunchTime time.Time
	Vehicle    string
	PatchUrl   string
	Info       string
}

type FlickrPhoto struct {
	ID    string `json:"id"`
	Owner string `json:"owner"`
	Title string `json:"title"`
	URLO  string `json:"url_o"`
	URLQ  string `json:"url_q"`
}
