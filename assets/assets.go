package assets

import (
	"embed"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"github.com/sirupsen/logrus"
	"golang.org/x/text/language"
	"io"
)

//go:embed bundles/*
var bundles embed.FS

type Localize func(string) string

func NewBundle(logger *logrus.Logger) map[string]Localize {
	i := make(map[string]Localize)
	dir, _ := bundles.ReadDir("bundles")
	bundle := i18n.NewBundle(language.English)
	bundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)
	for _, entry := range dir {
		open, _ := bundles.Open(fmt.Sprintf("bundles/%s", entry.Name()))
		file, _ := io.ReadAll(open)
		bundle.MustParseMessageFileBytes(file, entry.Name())

		//in "en.toml" [:2] is "en"
		lang := entry.Name()[:2]
		loc := i18n.NewLocalizer(bundle, lang)
		i[lang] = func(s string) string {
			str, err := loc.Localize(&i18n.LocalizeConfig{MessageID: s})
			if err != nil {
				logger.Warn(fmt.Sprintf("Unknown localizing string `%s` with lang `%s`", s, lang))
				return s
			}
			return str
		}
	}
	return i
}
