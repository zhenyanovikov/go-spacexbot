FROM ubuntu:latest as base
ADD main /app/
WORKDIR /app/
RUN apt update && apt install -y ca-certificates
CMD ["/app/main"]
