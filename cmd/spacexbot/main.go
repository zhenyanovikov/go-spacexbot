package main

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
	"go-spacexbot/assets"
	"go-spacexbot/internal/bot"
	"go-spacexbot/internal/bot/repository"
	"time"
)

type Config struct {
	TelegramToken string `envconfig:"TELEGRAM_TOKEN"`
	DBDriver      string `envconfig:"DB_DRIVER"`
	DBUrl         string `envconfig:"DB_URL"`

	TwitterAccessToken       string `envconfig:"TWITTER_ACCESS_TOKEN"`
	TwitterAccessTokenSecret string `envconfig:"TWITTER_ACCESS_TOKEN_SECRET"`
	TwitterApiKey            string `envconfig:"TWITTER_API_KEY"`
	TwitterSecretKey         string `envconfig:"TWITTER_SECRET_KEY"`
}

func main() {
	logger := logrus.New()
	bundles := assets.NewBundle(logger)
	config := NewConfig()

	repo, err := repository.New(config.DBDriver, config.DBUrl)
	if err != nil {
		panic(err)
	}

	tgBot, err := bot.NewTelegramBot(repo, logger, bundles, config.TelegramToken)
	if err != nil {
		panic(err)
	}
	tgBot.Start()
	for {
		time.Sleep(10 * time.Second)
	}
}

func NewConfig() *Config {
	cfg := new(Config)
	envconfig.MustProcess("app", cfg)
	return cfg
}
